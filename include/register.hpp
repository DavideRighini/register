#ifndef REGISTER_HPP
#define REGISTER_HPP

SC_MODULE(Register) {
  static const unsigned SIGN_SIZE = 16;

	sc_in<sc_lv<SIGN_SIZE> > din;
	sc_in<bool> ck;
	sc_in<bool> reset;
	sc_out<sc_lv<SIGN_SIZE> > dout;
	
	SC_CTOR(Register) {
		SC_THREAD(update_ports);
			sensitive << ck.pos() << reset;
		}
	
	private:
	
	void update_ports();
	unsigned data_temp;
	sc_lv<SIGN_SIZE> data_arr_temp;
};

#endif
