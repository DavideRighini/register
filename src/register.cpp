#include <systemc.h>
#include "register.hpp"

void Register::update_ports() {
  while(true) {
  	wait();
  	if (reset->read() == 1) {
			data_temp = 0;
			dout->write(data_temp);  
			wait(SC_ZERO_TIME);
		}
		else if (ck->read() ==1) {
			data_arr_temp = din;
			dout->write(data_arr_temp);
			wait(SC_ZERO_TIME);
		}
		
	}	
}

