#include <systemc.h>
#include "register.hpp"

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

static const unsigned TEST_SIZE = 6;
static const unsigned DATA_WIDTH = 16;
sc_fifo<int> observer_register_out;
sc_fifo<bool> observer_register_reset;
sc_fifo<bool> observer_ck;
int values1[TEST_SIZE]; //vettore usato per tutti i test

SC_MODULE(TestBench) 
{
 	public:
	sc_signal<sc_lv<DATA_WIDTH> > reg_din;
	sc_signal<bool> reg_ck;
	sc_signal<bool> reg_reset;
	sc_signal<sc_lv<DATA_WIDTH> > reg_dout; 

	Register prog_count; //REGISTRO1

  SC_CTOR(TestBench) : prog_count("prog_count")
  {
 		init_values();

		SC_THREAD(clock_thread);
		SC_THREAD(init_values_register_test);
		prog_count.din(this->reg_din); 
		prog_count.dout(this->reg_dout);
		prog_count.ck(this->reg_ck);
		prog_count.reset(this->reg_reset);	
		SC_THREAD(observer_thread_register);
			sensitive << prog_count.ck << prog_count.dout;

  }

	void init_values() {
				values1[0] = 5;
				values1[1] = 4;
				values1[2] = 3;
				values1[3] = 2;
				values1[4] = 1;
				values1[5] = 0;
						  		  
	}

	void clock_thread() {
		bool value = false;
		while(true) {
			reg_ck.write(value);
			value = !value;
			wait(10,SC_NS);
		}
	}

	void init_values_register_test() {
		for (unsigned i=0;i<TEST_SIZE;i++) {
		  reg_din.write(values1[i]);   
		  wait(10,SC_NS);
		}
		wait(50,SC_NS);
		sc_stop();
	}

	void observer_thread_register() {
		while(true) {
				wait();
				int value = prog_count.dout->read().to_uint();
				bool value1 = prog_count.reset->read();
				bool value2 = prog_count.ck->read();
				cout << "observer_thread: at " << sc_time_stamp() << " reg out: " << value << " ck: " << value2 << " reset: " << value1 << endl;
				if (observer_register_out.num_free()>0 ) observer_register_out.write(value);
				if (observer_register_reset.num_free()>0 ) observer_register_reset.write(value1);
				if (observer_ck.num_free()>0 ) observer_ck.write(value2);
		}
	}


	int check_register() {
		int r_out,r_out_prec=0,r_ck,r_reset;
		for (unsigned i=0;i<TEST_SIZE;i++) {
				r_out=observer_register_out.read(); //porta fifo
				r_reset=observer_register_reset.read(); //porta fifo
				r_ck=observer_ck.read(); //porta fifo
		    if (i!=0) { 
		    	if(r_ck == 0 & r_out!=r_out_prec) return 1;
		    }
		    r_out_prec = r_out;
		}               

		return 0;
	}


};

int sc_main(int argc, char* argv[]) {

	TestBench testbench("testbench");
	sc_start();

  return testbench.check_register();
}
